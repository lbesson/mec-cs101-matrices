Things to do for this project
=============================
This project is done, there is not much to do.

--------------------------------------------------------------------------

About this file
---------------
In case that some part of your project is not done (not completed yet), you can explain here what still has to be done.

Imagine that some other team would have to work on your project, and conclude it, well then this file should be as helpful for them as possible (while not being too long or verbous).
